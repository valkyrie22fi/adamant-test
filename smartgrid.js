var smartgrid = require('smart-grid');

var settings = {
    outputStyle: 'scss', /* less || scss || sass || styl */
    columns: 12, /* number of grid columns */
    offset: '15px', /* gutter width px || % */
    mobileFirst: false, /* mobileFirst ? 'min-width' : 'max-width' */
    oldSizeStyle: false,
    container: {
        maxWidth: '1170px', /* max-width оn very large screen */
        fields: '15px' /* side fields */
    },
    breakPoints: {
        lg: {
            width: '1170px' /* -> @media (max-width: 1100px) */
        },
        md: {
            width: '768px',
        },
        sm: {
            width: '460px',
        },
        xs: {
            width: '320px',
        }
    }
};

smartgrid('./src/scss', settings);